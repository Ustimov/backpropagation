﻿open System
open System.Drawing
open System.IO
open System.Diagnostics

type NeuralNetwork(numberOfInput, numberOfHidden, numberOfOutput) =
    let ihWeight: float[][] = Array.zeroCreate numberOfHidden 
                              |> Seq.map (fun i -> Array.zeroCreate numberOfInput) 
                              |> Seq.toArray
    let hoWeight: float[][] = Array.zeroCreate numberOfOutput
                              |> Seq.map (fun i -> Array.zeroCreate numberOfHidden)
                              |> Seq.toArray
    let hBias: float[] = Array.zeroCreate numberOfHidden
    let oBias: float[] = Array.zeroCreate numberOfOutput
    let hGrad: float[] = Array.zeroCreate numberOfHidden
    let oGrad: float[] = Array.zeroCreate numberOfOutput
    let hOutput: float[] = Array.zeroCreate numberOfHidden
    let oOutput: float[] = Array.zeroCreate numberOfOutput
    let ihPrevWeightDelta: float[][] = Array.zeroCreate numberOfHidden
                                       |> Seq.map (fun i -> Array.zeroCreate numberOfInput)
                                       |> Seq.toArray
    let hoPrevWeightDelta: float[][] = Array.zeroCreate numberOfOutput
                                       |> Seq.map (fun i -> Array.zeroCreate numberOfHidden)
                                       |> Seq.toArray
    let hPrevBiasDelta: float[] = Array.zeroCreate numberOfHidden
    let oPrevBiasDelta: float[] = Array.zeroCreate numberOfOutput
    let random = Random()

    let softmax (value: float[]) =
        let max = Seq.max value
        let scale = Seq.fold (fun state v -> state + Math.Exp(v - max)) 0.0 value
        let result = Array.zeroCreate value.Length
        for i in 0..result.Length - 1 do
            result.[i] <- Math.Exp(value.[i] - max) / scale
        result

    let shuffle(array: int[]) = 
        for i in 0..array.Length - 1 do
            let r = random.Next(i, array.Length)
            let tmp = array.[r]
            array.[r] <- array.[i]
            array.[i] <- tmp
        array

    member this.FeedForward(input: float[]) = 
        let ih: float[] = Array.zeroCreate numberOfHidden
        for i in 0..ihWeight.Length - 1 do
            for j in 0..ihWeight.[i].Length - 1 do
                ih.[i] <- ih.[i] + input.[j] * ihWeight.[i].[j]
            ih.[i] <- ih.[i] + hBias.[i]
            ih.[i] <- Math.Tanh(ih.[i])
            hOutput.[i] <- ih.[i]
        let ho: float[] = Array.zeroCreate numberOfOutput
        for i in 0..hoWeight.Length - 1 do
            for j in 0..hoWeight.[i].Length - 1 do
                ho.[i] <- ho.[i] + ih.[j] * hoWeight.[i].[j]
            ho.[i] <- ho.[i] + oBias.[i]
        let activated = softmax ho
        for i in 0..activated.Length - 1 do
            oOutput.[i] <- activated.[i]
        activated

    member this.BackPropagation(input: float[], target: float[], alpha: float, momentum: float) = 
        for i in 0..oGrad.Length - 1 do
            let derivative = oOutput.[i] * (1.0 - oOutput.[i])
            oGrad.[i] <- derivative * (target.[i] - oOutput.[i])
        for i in 0..hGrad.Length - 1 do
            let derivative = (1.0 - hOutput.[i]) * (1.0 + hOutput.[i])
            let mutable sum = 0.0
            for j in 0..numberOfOutput - 1 do
                sum <- sum + oGrad.[j] * hoWeight.[j].[i]
            hGrad.[i] <- derivative * sum
        for i in 0..ihWeight.Length - 1 do
            let len = ihWeight.[i].Length
            for j in 0..ihWeight.[i].Length - 1 do
                let delta = alpha * hGrad.[i] * input.[j]
                ihWeight.[i].[j] <- ihWeight.[i].[j] + delta
                ihWeight.[i].[j] <- ihWeight.[i].[j] + momentum * ihPrevWeightDelta.[i].[j]
                ihPrevWeightDelta.[i].[j] <- delta
        for i in 0..hBias.Length - 1 do
            let delta = alpha * hGrad.[i]
            hBias.[i] <- hBias.[i] + delta
            hBias.[i] <- hBias.[i] + momentum * hPrevBiasDelta.[i]
            hPrevBiasDelta.[i] <- delta
        for i in 0..hoWeight.Length - 1 do
            for j in 0..hoWeight.[i].Length - 1 do
                let delta = alpha * oGrad.[i] * hOutput.[j]
                hoWeight.[i].[j] <- hoWeight.[i].[j] + delta
                hoWeight.[i].[j] <- hoWeight.[i].[j] + momentum * hoPrevWeightDelta.[i].[j]
                hoPrevWeightDelta.[i].[j] <- delta
        for i in 0..oBias.Length - 1 do
            let delta = alpha * oGrad.[i]
            oBias.[i] <- oBias.[i] + delta
            oBias.[i] <- oBias.[i] + momentum * oPrevBiasDelta.[i]
            oPrevBiasDelta.[i] <- delta
    
    member this.Train(input: float[][], target: float[][], alpha: float, momentum: float, maxEpochs: int) = 
        for j in 0..maxEpochs - 1 do
            let sequence = [for i in 0..input.Length - 1 -> i] |> Seq.toArray |> shuffle
            for i in 0..input.Length - 1 do
                this.FeedForward(input.[sequence.[i]]) |> ignore
                this.BackPropagation(input.[sequence.[i]], target.[sequence.[i]], alpha, momentum)

    member this.SetWeights(weights: float[]) = 
        let mutable k = 0
        for i in 0..numberOfInput - 1 do
            for j in 0..numberOfHidden - 1 do
                ihWeight.[j].[i] <- weights.[k]
                k <- k + 1
        for i in 0..numberOfHidden - 1 do
            hBias.[i] <- weights.[k]
            k <- k + 1
        for i in 0..numberOfHidden - 1 do
            for j in 0..numberOfOutput - 1 do
                hoWeight.[j].[i] <- weights.[k]
                k <- k + 1
        for i in 0..numberOfOutput - 1 do
            oBias.[i] <- weights.[k]
            k <- k + 1

let loadInputFrom path =
    let files = Directory.GetFiles(path)
    let input = ResizeArray<float[]>();
    let digit = ResizeArray<int>()
    let black = Color.FromArgb(255, 0, 0, 0)
    for file in files do
        use bitmap = new Bitmap(file)
        let array: float[] = Array.zeroCreate (bitmap.Height * bitmap.Width)
        for i in 0..bitmap.Height - 1 do
            for j in 0..bitmap.Width - 1 do
                array.[j + i * bitmap.Width] <-
                    if bitmap.GetPixel(j, i) = black then 1.0 else 0.0
        input.Add(array)
        digit.Add(int(Path.GetFileName(file).Substring(0, 1)))
    input, digit

let prepareData bitmap digit =
    let map =
        Seq.map
            (fun d ->
                let a: float[] = Array.zeroCreate 10
                a.[d] <- 1.0; a) digit
    Seq.toArray bitmap, Seq.toArray map

[<EntryPoint>]
let main argv = 
    let numberOfInput = 35
    let numberOfHidden = 50
    let numberOfOutput = 10

    let neuralNetwork = NeuralNetwork(numberOfInput, numberOfHidden, numberOfOutput)

    let bitmap, digit = loadInputFrom "../../../Images/train"
    let input, target = prepareData bitmap digit

    let alpha = 0.001
    let momentum = 0.01
    let maxEpochs = 10000

    neuralNetwork.Train(input, target, alpha, momentum, maxEpochs)

    let input, _ = loadInputFrom "../../../Images/test3"
    let testInput, _ = prepareData input [||]

    for i in testInput do
        let result = neuralNetwork.FeedForward(i)
        printfn "%d %A" (Array.IndexOf(result, Array.max(result)) + 1) result

    0
