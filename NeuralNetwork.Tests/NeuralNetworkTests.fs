﻿module NeuralNetwork.Tests

open NUnit.Framework
open Program

[<TestFixture>]
type NeuralNetworkTests() =

    [<Test>]
    member this.TestFeedForward() = 
        let neuralNetwork = NeuralNetwork(3, 4, 2)
        let weights = [| 0.01; 0.02; 0.03; 0.04; 0.05; 0.06; 0.07; 0.08; 0.09; 0.10; 0.11; 0.12; 0.13; 0.14; 0.15; 0.16; 0.17; 0.18; 0.19; 0.20; 0.21; 0.22; 0.23; 0.24; 0.25; 0.26 |]
        neuralNetwork.SetWeights(weights)
        let input = [| 1.0; 2.0; 3.0 |]
        let output = neuralNetwork.FeedForward(input)
        Assert.AreEqual(0.4920, output.[0], 0.001)
        Assert.AreEqual(0.5080, output.[1], 0.001)